
# Test technique entretien BDV

## :construction: Installation
Après avoir récupérer le répertoire, il faudra installer les dépendences du projet avec

    composer install

Une fois l'installation terminée, il faudra lancer le projet avec

    symfony serve

## :speech_balloon: Présentation
Le but de ce test est de reproduire (en version desktop uniquement) les pages compagnies du site Bourse des Vols.

Idéalement, vous devez refaire le gabarit de deux pages :
- [EasyJet](https://www.bourse-des-vols.com/easyjet.php)
- [Air France](https://www.bourse-des-vols.com/air-france.php)

Vous devez reproduire le contenu suivant :
- Fil d'Ariane
- Blocs prix
- Découverte compagnie
- Bloc "Vols" (à droite)
- Bloc "fiche d'identité" (à droite)

## :gift: Contraintes techniques
- Créer une branche "entretien-PRENOM" et travailler dessus.
- Un seul controller.
- Un seul fichier de template.
- La route doit être /compagnies-aeriennes/XXX (où XXX est le nom de la compagnie aérienne, ex: air-france)

## :pushpin: Déroulement
Vous avez un profil plutôt orienté Back-End ? Vous devrez retourner sous format JSON les données de la page appelée.<br/>
Vous avez un profil plutôt orienté Front-End ? Vous devrez faire l'intégration des pages avec le framework de votre choix (idéalement NuxtJS).<br/>
Le Front et le Back ne vous fait pas peur ? Mettez-nous des paillettes dans les yeux :sparkles:

## :memo: Notes
:computer: Nous ne sommes pas des sauvages, évidemment que Google est ton ami si besoin.<br/>
:warning: Vous n'êtes pas obligé de reproduire à 100% ce qui est demandé.<br/>
:warning: L'intégration ne doit pas forcément être parfaite.<br/>
:sparkling_heart: Bonus si troisième compagnie.<br/>
:hourglass_flowing_sand: Vous disposez d'un temps de 45min à 1h.

Bonne chance ! :muscle: